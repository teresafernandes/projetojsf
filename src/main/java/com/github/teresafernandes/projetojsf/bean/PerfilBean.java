package com.github.teresafernandes.projetojsf.bean;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.github.teresafernandes.projetojsf.modelo.Perfil;
import com.github.teresafernandes.projetojsf.modelo.Permissao;
import com.github.teresafernandes.projetojsf.servico.PerfilServico;
import com.github.teresafernandes.projetojsf.servico.PermissaoServico;
import com.github.teresafernandes.projetojsf.util.Mensagem;
import com.github.teresafernandes.projetojsf.util.ValidatorUtil;


/**
 * @author Teresa Fernandes
 * */

@Controller
@Scope("view")
public class PerfilBean extends CrudBean<Perfil, PerfilServico>{

	private static final long serialVersionUID = 1L;
	
	private Permissao permissao;
	@Autowired
	private PermissaoServico permissaoServico;
	
	@Inject
	public PerfilBean(PerfilServico servico) {
		super(servico);
	}
	
	/**
     * Método que pode ser utilizado para carregar dados de serviços auxiliares.
     */
    @PostConstruct
    public void init() {
    }
    	
	public List<Perfil> getTodosPerfis(){
		return getServico().listarTodos();
	}
	
	@Override
	protected void aposCarregarAcaoEditar(Perfil objEdicao) {
		getObj().setPermissoes(permissaoServico.obterPermissoesPeloPerfil(objEdicao));
		permissao = null;
	}
	
	public void removerPermissao(Permissao permissao){
		if(getObj().getPermissoes() != null){
			getObj().getPermissoes().remove(permissao);
		}
	}
	
	public void adicionarPermissao(){
		if(ValidatorUtil.isEmpty(permissao))
			Mensagem.mensagemErro("Selecione uma permissão.");
		else{
			if(getObj().getPermissoes() == null)
				getObj().setPermissoes(new ArrayList<Permissao>());
			getObj().getPermissoes().add(permissao);
			permissao = null;
		}
	}
	
	public Permissao getPermissao() {
		return permissao;
	}

	public void setPermissao(Permissao permissao) {
		this.permissao = permissao;
	}
	
}
