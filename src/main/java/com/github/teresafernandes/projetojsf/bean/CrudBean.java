/**
 * Data: 22/01/2016
 */
package com.github.teresafernandes.projetojsf.bean;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.github.teresafernandes.projetojsf.exception.ServiceBusinessException;
import com.github.teresafernandes.projetojsf.modelo.EntidadePersistente;
import com.github.teresafernandes.projetojsf.servico.GenericService;
import com.github.teresafernandes.projetojsf.util.Mensagem;

/**
 * @author Teresa Fernandes
 *
 */
public abstract class CrudBean<T extends EntidadePersistente, S extends GenericService<T>> implements Serializable{

	private static final long serialVersionUID = 1L;

	public enum Pagina {
        FORMULARIO, LISTAGEM, PERSONALIZADA;
    }

    public enum Acao {
        INSERIR, EDITAR;
    }
    
	private T obj;
	private T objBusca;
//	private List<T> listaObj;
	private S servico;
	private Class<T> classePersistente;
	private LazyDataModel<T> dataModel;
	private Pagina pagina;
	private Acao acao;
	private boolean contadorDesatualizado;
	
	@SuppressWarnings("unchecked")
	public CrudBean(S servico) {

		classePersistente = (Class<T>) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[0];
		this.servico = servico;

		novoObj();
		novoObjBusca();
		
		definirModoInicial();
        initializarDataModel();
    }
	
	protected void definirModoInicial() {
		carrecarAcaoBusca();
	}
	
	@SuppressWarnings("serial")
	protected void initializarDataModel() {
        dataModel = new LazyDataModel<T>() {
        	Long total = null;
            Boolean buscaExecutada = false;

            @Override
            public List<T> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, String> filters) {
                buscaExecutada = true;
                
                sortField = (sortOrder != null && SortOrder.UNSORTED.equals(sortOrder)) ? null : sortField;
                boolean sort = (sortOrder != null && SortOrder.ASCENDING.equals(sortOrder));
                
                return carregarDadosBusca(first, pageSize, sortField, sort);
            }

            @Override
            public int getRowCount() {
                if (total == null || buscaExecutada || contadorDesatualizado) {
                    buscaExecutada = false;
                    contadorDesatualizado = false;
                    total = carregarContadorBusca();
                }
                return total.intValue();
            }
            
            @Override
            public void setRowIndex( int rowIndex ) {

               if ( rowIndex == -1 || getPageSize() == 0 ) {
                super.setRowIndex( -1 );
               } else
                super.setRowIndex( rowIndex % getPageSize() );
            }
            
            @Override
            public void setWrappedData(Object list) {
                super.setWrappedData(list);
                aposBuscar();
            };

        };

        dataModel.setPageSize(10);
	}

	protected void novoObj() {
       try {
           obj = classePersistente.newInstance();
       } catch (InstantiationException e) {
           Mensagem.mensagemErro(e.getMessage());
       } catch (IllegalAccessException e) {
    	   Mensagem.mensagemErro(e.getMessage());
       }
	}
	
	protected void novoObjBusca() {
	       try {
	           objBusca = classePersistente.newInstance();
	       } catch (InstantiationException e) {
	           Mensagem.mensagemErro(e.getMessage());
	       } catch (IllegalAccessException e) {
	    	   Mensagem.mensagemErro(e.getMessage());
	       }
		}
	
	protected List<T> carregarDadosBusca(int first, int pageSize, String sortField, boolean sortOrder) {
		return servico.buscarPorAtributosEntidade(objBusca, first, pageSize, sortField, sortOrder);
	}
	
	protected Long carregarContadorBusca() {
		return servico.buscarContadorPorAtributosEntidade(objBusca);
	}

    public boolean isPaginaFormulario() {
        return pagina == Pagina.FORMULARIO;
    }

    public boolean isPaginaListagem() {
    	return pagina == Pagina.LISTAGEM;
    }

    public boolean isPaginaPersonalizada() {
    	return pagina == Pagina.PERSONALIZADA;
    }

    public boolean isAcaoEditar() {
        return acao == Acao.EDITAR;
    }

    public boolean isAcaoInserir() {
    	return acao == Acao.INSERIR;
    }

    protected void antesCarregarAcaoInserir() { }
    protected void aposCarregarAcaoInserir() { }
    
    protected void antesBuscar() { }
    protected void aposBuscar() { }
    
    protected void antesCarregarAcaoBuscar() { }
    protected void aposCarregarAcaoBuscar() { }
    
    protected void antesCarregarAcaoEditar( T objEdicao) { }
    protected void aposCarregarAcaoEditar(T objEdicao) { }

    protected void antesExcluir() { }
    protected void aposExcluir() { }

    protected void antesSalvar(T objInsercao) { }
    protected void aposSalvar(T objInsercao) { }
	
    public void carregarAcaoInserir() {
        try {
            antesCarregarAcaoInserir();

            setPagina(Pagina.FORMULARIO);
            setAcao(Acao.INSERIR);
            novoObj();

            aposCarregarAcaoInserir();
        } catch (Exception e) {
            Mensagem.mensagemErro(e.getMessage());
        }
    }
    
    public void buscar() {
        try {
            antesBuscar();

            setPagina(Pagina.LISTAGEM);
            contadorDesatualizado = true;
        } catch (Exception e) {
            Mensagem.mensagemErro(e.getMessage());
        }
    }
    
    public void carrecarAcaoBusca() {
        try {
            antesCarregarAcaoBuscar();

            novoObjBusca();
            setPagina(Pagina.LISTAGEM);
            aposCarregarAcaoBuscar();

            buscar();
        } catch (Exception e) {
            Mensagem.mensagemErro(e.getMessage());
        }
    }
    
    public void carregarAcaoEditar(T objEdicao) {
        try {
            antesCarregarAcaoEditar(objEdicao);

            setObj(objEdicao);
            setPagina(Pagina.FORMULARIO);
            setAcao(Acao.EDITAR);

            aposCarregarAcaoEditar(objEdicao);
        } catch (Exception e) {
            Mensagem.mensagemErro(e.getMessage());
        }
    }
    
    public void excluir() {
        excluir(obj);
    }
    
    public void excluir(T objExclusao) {
        setPagina(Pagina.LISTAGEM);
        try {
            antesExcluir();
            
            executarServicoExclusao(objExclusao);
            contadorDesatualizado = true;
            Mensagem.mensagemInformacao("Exclusão realizada com sucesso");

            aposExcluir();
        }  catch (ServiceBusinessException e) {
            Mensagem.mensagemErro(e.getMessage());
        }
    }
    
    public void salvar(T objInsercao, Acao acaoAposSalvar) {
        try {
            antesSalvar(objInsercao);
            if (acao == Acao.INSERIR) {
                executarServicoInsercao(objInsercao);
                Mensagem.mensagemInformacao("Registro inserido com sucesso");
            } else if (acao == Acao.EDITAR) {
                executarServicoEdicao(objInsercao);
                Mensagem.mensagemInformacao("Registro editado com sucesso");
            }
            aposSalvar(objInsercao);

            if (acaoAposSalvar == Acao.INSERIR) {
                carregarAcaoInserir();
            } else {
                setAcao(Acao.EDITAR);
            }

        } catch (ServiceBusinessException e) {
            Mensagem.mensagemErro(e.getMessage());
        }

    }
    
    public void executarServicoInsercao(T objInsercao) throws ServiceBusinessException {
        servico.salvar(objInsercao);
    }

    public void executarServicoEdicao(T objEdicao) throws ServiceBusinessException{
        servico.salvar(objEdicao);
    }

    public void executarServicoExclusao(T objExclusao) throws ServiceBusinessException {
        servico.excluir(objExclusao);
    }
    
    public void salvarEContinuar() {
        salvar(obj, Acao.EDITAR);
    }
    
    public void salvarELimpar() {
        salvar(obj, Acao.INSERIR);
    }
    
    public void salvarEVoltar() {
        salvar(obj, Acao.EDITAR);
        voltarPaginaListagem();
    }
    
    public void voltarPaginaListagem(){
    	carrecarAcaoBusca();	
    }
	
	public List<T> getTodosObj(){
		return servico.listarTodos();
	}
		
	public T getObj() {
		return obj;
	}

	public void setObj(T obj) {
		this.obj = obj;
	}

	public T getObjBusca() {
		return objBusca;
	}

	public void setObjBusca(T objBusca) {
		this.objBusca = objBusca;
	}

	public S getServico() {
		return servico;
	}

	public void setServico(S servico) {
		this.servico = servico;
	}

	public Class<T> getClassePersistente() {
		return classePersistente;
	}

	public void setClassePersistente(Class<T> classePersistente) {
		this.classePersistente = classePersistente;
	}

	public LazyDataModel<T> getDataModel() {
		return dataModel;
	}

	public void setDataModel(LazyDataModel<T> dataModel) {
		this.dataModel = dataModel;
	}

	public Pagina getPagina() {
		return pagina;
	}

	public void setPagina(Pagina pagina) {
		this.pagina = pagina;
	}

	public Acao getAcao() {
		return acao;
	}

	public void setAcao(Acao acao) {
		this.acao = acao;
	}

}
