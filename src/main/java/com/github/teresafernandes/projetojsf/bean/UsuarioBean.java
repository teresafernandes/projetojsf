package com.github.teresafernandes.projetojsf.bean;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.github.teresafernandes.projetojsf.modelo.Usuario;
import com.github.teresafernandes.projetojsf.servico.PermissaoServico;
import com.github.teresafernandes.projetojsf.servico.UsuarioServico;

/**
 * @author Teresa Fernandes
 * */

@Controller
@Scope("view")
public class UsuarioBean extends CrudBean<Usuario, UsuarioServico>{

	private static final long serialVersionUID = 1L;

	@Autowired
	private PermissaoServico permissaoServico;
		
	@Inject
	public UsuarioBean(UsuarioServico servico) {
		super(servico);
	}
	
	/**
     * Método que pode ser utilizado para carregar dados de serviços auxiliares.
     */
    @PostConstruct
    public void init() {
    }
    
	@Override
	protected void aposCarregarAcaoEditar(Usuario objEdicao) {
		getObj().getPerfil().setPermissoes(permissaoServico.obterPermissoesPeloUsuario(objEdicao));
	}
		
}
