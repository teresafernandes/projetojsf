package com.github.teresafernandes.projetojsf.bean;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.github.teresafernandes.projetojsf.modelo.Permissao;
import com.github.teresafernandes.projetojsf.servico.PermissaoServico;


/**
 * @author Teresa Fernandes
 * */

@Controller
@Scope("view")
public class PermissaoBean extends CrudBean<Permissao, PermissaoServico>{


	private static final long serialVersionUID = 1L;

	@Inject
	public PermissaoBean(PermissaoServico servico) {
		super(servico);
	}
	
	/**
     * Método que pode ser utilizado para carregar dados de serviços auxiliares.
     */
    @PostConstruct
    public void init() {
    }

    public List<Permissao> getTodasPermissoes(){
		return getServico().listarTodos();
	}
}
