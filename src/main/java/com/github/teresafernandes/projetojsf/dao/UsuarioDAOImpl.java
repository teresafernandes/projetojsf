package com.github.teresafernandes.projetojsf.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.github.teresafernandes.projetojsf.modelo.Usuario;

/**
 * @author Teresa Fernandes
 * */

@Repository
public class UsuarioDAOImpl extends GenericDAOImpl<Usuario> implements UsuarioDAO{
	private static final long serialVersionUID = 1L;
	
	@SuppressWarnings("unchecked")
	public Usuario obterUsuarioPeloLogin(String login) {
		List<Usuario> usuarios = getEntityManager().createQuery(
				"from Usuario where login = :login")
				.setParameter("login", login)
				.getResultList();
		
		if(usuarios.isEmpty()){
			return null;
		}
		
		return usuarios.get(0);
	}
}
