package com.github.teresafernandes.projetojsf.dao;

import com.github.teresafernandes.projetojsf.modelo.Perfil;

/**
 * @author Teresa Fernandes
 * */

public interface PerfilDAO extends GenericDAO<Perfil>{

	public Long countPerfisPeloNome(Perfil p);
}
