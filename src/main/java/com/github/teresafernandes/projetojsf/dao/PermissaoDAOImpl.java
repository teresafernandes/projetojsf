package com.github.teresafernandes.projetojsf.dao;

import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.github.teresafernandes.projetojsf.exception.DataAccessException;
import com.github.teresafernandes.projetojsf.modelo.Perfil;
import com.github.teresafernandes.projetojsf.modelo.Permissao;
import com.github.teresafernandes.projetojsf.modelo.Usuario;

/**
 * @author Teresa Fernandes
 * */

@Repository
public class PermissaoDAOImpl extends GenericDAOImpl<Permissao> implements PermissaoDAO{

	private static final long serialVersionUID = 1L;

	@Override
	public List<Permissao> obterPermissoesPeloUsuario(Usuario usuario) {
		return obterPermissoesPeloPerfil(usuario.getPerfil());
	}
	
	@SuppressWarnings("unchecked")
	public List<Permissao> obterPermissoesPeloPerfil(Perfil perfil){
		try{
			List<Permissao> p = entityManager.createQuery(
					  " select p from PermissaoPerfil as pp "
					+ " join pp.permissao as p"
					+ " where pp.perfil.id = :perfil")
					.setParameter("perfil", perfil.getId()).getResultList();
			return p;
		} catch (org.springframework.dao.DataAccessException e) {
            throw new DataAccessException(e);
        }
	}

	
	public Long countPermissoesPeloNome(Permissao permissao) {
		try{
			String hql = "select count(p.id) from Permissao p "
					+ " where p.nome = :nome"
					+ " and p.id <> :id";
			Query q = entityManager.createQuery(hql);
			q.setParameter("nome", permissao.getNome());
			q.setParameter("id", permissao.getId());
			
			return (Long) q.getSingleResult();
		} catch (org.springframework.dao.DataAccessException e) {
            throw new DataAccessException(e);
        }
	}

	@Override
	public List<?> buscarPorAtributosEntidade(Permissao objBusca, int first,
			int pageSize, boolean isCount) {
		String hql = "SELECT "+ (isCount ? " count(p.id) " : " p ");
    	hql+=" FROM Permissao p WHERE 1=1  ";
    	Query q = entityManager.createQuery(hql);
    	
    	if(!isCount){
    		q.setMaxResults(pageSize);
    		q.setFirstResult(first);
        }
     	return q.getResultList();
	}

}
