package com.github.teresafernandes.projetojsf.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.github.teresafernandes.projetojsf.exception.DataAccessException;
import com.github.teresafernandes.projetojsf.modelo.Perfil;
import com.github.teresafernandes.projetojsf.modelo.PermissaoPerfil;

/**
 * @author Teresa Fernandes
 * */

@Repository
public class PermissaoPerfilDAOImpl extends GenericDAOImpl<PermissaoPerfil> implements PermissaoPerfilDAO{

	private static final long serialVersionUID = 1L;

	@SuppressWarnings("unchecked")
	public List<PermissaoPerfil> obterPermissoesPeloPerfil(Perfil perfil){
		try{
			List<PermissaoPerfil> p = entityManager.createQuery(
					  " select pp from PermissaoPerfil as pp "
					+ " join pp.permissao as p"
					+ " where pp.perfil.id = :perfil")
					.setParameter("perfil", perfil.getId()).getResultList();
			return p;
		} catch (org.springframework.dao.DataAccessException e) {
            throw new DataAccessException(e);
        }
	}

}
