package com.github.teresafernandes.projetojsf.dao;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.github.teresafernandes.projetojsf.exception.DataAccessException;
import com.github.teresafernandes.projetojsf.exception.DataAccessExceptionTranslator;
import com.github.teresafernandes.projetojsf.modelo.EntidadePersistente;

/**
 * @author Teresa Fernandes
 * */

public class GenericDAOImpl<T extends EntidadePersistente> implements GenericDAO<T> {

	private static final long serialVersionUID = 1L;

	private final Class<T> classe;
	
	@PersistenceContext
	protected EntityManager entityManager;
	
	@SuppressWarnings("unchecked")
	public GenericDAOImpl(){
		this.classe = (Class<T>) ((ParameterizedType)
				getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}
	
	public void salvar(T t){
		try{
			if(t.getId() == null || t.getId() == 0)
				entityManager.persist(t);
			else
				entityManager.merge(t);
		} catch (org.springframework.dao.DataAccessException e) {
            throw DataAccessExceptionTranslator.translate(e);
        } catch (Exception e) {
            throw new DataAccessException(e);
        }
	}
	
	public void excluir(T t){
		try{
			entityManager.remove(t);			
		}catch (org.springframework.dao.DataAccessException e) {
            throw DataAccessExceptionTranslator.translate(e);
		} catch (Exception e) {
			throw new DataAccessException(e);
		}
	}
	
	public List<T> listarTodos(){
		try{
			List<T> resultados = entityManager.createQuery(
					"select c from " + classe.getName() + " c", classe).getResultList();
			return resultados;
		} catch (Exception e) {
            throw new DataAccessException(e);
        }
	}
	
	public T buscarPorId(Integer id){
		try{
			T t = entityManager.createQuery("from " + classe.getName() + " where id = :cod", classe)
					.setParameter("cod", id).getSingleResult();
			
			return t;
		} catch (org.springframework.dao.DataAccessException e) {
            throw new DataAccessException(e);
        }
	}
	
	public Class<T> getClassePersistente(){
		return classe;
	}

	public EntityManager getEntityManager() {
		return entityManager;
	}
	
	public List<?> buscarPorAtributosEntidade(T objBusca, int first, int pageSize, boolean isCount){
		String hql = "SELECT "+ (isCount ? " count(p.id) " : " p ");
    	hql+=" FROM "+getClassePersistente().getName()+" p WHERE 1=1  ";
    	Query q = entityManager.createQuery(hql);
    	
    	if(!isCount){
    		q.setMaxResults(pageSize);
    		q.setFirstResult(first);
        }
     	return q.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<T> buscarPorAtributosEntidade(T objBusca, int first, int pageSize, String sortField, boolean sortOrder){
    	return (List<T>) buscarPorAtributosEntidade(objBusca, first, pageSize, false);
    }
    
    public Long buscarContadorPorAtributosEntidade(T objBusca){
    	Long resultado =  (Long) buscarPorAtributosEntidade(objBusca, 0,0,true).get(0);
        return Long.parseLong(resultado.toString());
    }
}
