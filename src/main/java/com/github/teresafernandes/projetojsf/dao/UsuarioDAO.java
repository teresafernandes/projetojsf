package com.github.teresafernandes.projetojsf.dao;

import com.github.teresafernandes.projetojsf.modelo.Usuario;

/**
 * @author Teresa Fernandes
 * */ 

public interface UsuarioDAO extends GenericDAO<Usuario>{

	public Usuario obterUsuarioPeloLogin(String login);
}
