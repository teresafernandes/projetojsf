package com.github.teresafernandes.projetojsf.dao;

import java.util.List;

import com.github.teresafernandes.projetojsf.modelo.Perfil;
import com.github.teresafernandes.projetojsf.modelo.PermissaoPerfil;

/**
 * @author Teresa Fernandes
 * */

public interface PermissaoPerfilDAO extends GenericDAO<PermissaoPerfil>{

	public List<PermissaoPerfil> obterPermissoesPeloPerfil(Perfil perfil);
}
