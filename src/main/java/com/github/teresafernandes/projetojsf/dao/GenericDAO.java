package com.github.teresafernandes.projetojsf.dao;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;

import com.github.teresafernandes.projetojsf.modelo.EntidadePersistente;

/**
 * @author Teresa Fernandes
 * */

public interface GenericDAO<T extends EntidadePersistente> extends Serializable {
	public void salvar(T t);
	public void excluir(T t);
	public List<T> listarTodos();
	public T buscarPorId(Integer id);
	public Class<T> getClassePersistente();
	public EntityManager getEntityManager();
    public List<T> buscarPorAtributosEntidade(T objBusca, int first, int pageSize, String sortField, boolean sortOrder) ;
    public Long buscarContadorPorAtributosEntidade(T objBusca) ;
    public List<?> buscarPorAtributosEntidade(T objBusca, int first, int pageSize, boolean isCount);
}
