package com.github.teresafernandes.projetojsf.dao;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.github.teresafernandes.projetojsf.exception.DataAccessException;
import com.github.teresafernandes.projetojsf.modelo.Perfil;

/**
 * @author Teresa Fernandes
 * */

@Repository
public class PerfilDAOImpl extends GenericDAOImpl<Perfil> implements PerfilDAO{

	private static final long serialVersionUID = 1L;

	public Long countPerfisPeloNome(Perfil perfil){
		try{
			String hql = "select count(p.id) from Perfil p "
					+ " where p.nome = :nome"
					+ " and p.id <> :id";
			Query q = entityManager.createQuery(hql);
			q.setParameter("nome", perfil.getNome());
			q.setParameter("id", perfil.getId());
			
			return (Long) q.getSingleResult();
		} catch (org.springframework.dao.DataAccessException e) {
            throw new DataAccessException(e);
        }
	}

}
