package com.github.teresafernandes.projetojsf.converter;

import java.util.Map;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.github.teresafernandes.projetojsf.modelo.EntidadePersistente;

/**
 * @author Teresa Fernandes
 * */

@FacesConverter("entityConverter")
public  class EntityConverter<T extends EntidadePersistente> implements Converter {
	
    @Override
    public Object getAsObject(FacesContext ctx, UIComponent component, String value) {
        if (value != null) {
            return this.getAttributesFrom(component).get(value);
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext ctx, UIComponent component, Object value) {
        if (value != null && !"".equals(value)) {
            EntidadePersistente entity = (EntidadePersistente) value;
            // adiciona item como atributo do componente
            this.addAttribute(component, entity);

            Integer codigo = entity.getId();
            if (codigo != null) {
                return String.valueOf(codigo);
            }
        }

        return (String) value;
    }

    protected void addAttribute(UIComponent component, EntidadePersistente persistent) {
        String key = Integer.toString(persistent.getId());
        this.getAttributesFrom(component).put(key, persistent);
    }

    protected Map<String, Object> getAttributesFrom(UIComponent component) {
        return component.getAttributes();
    }
	
}
