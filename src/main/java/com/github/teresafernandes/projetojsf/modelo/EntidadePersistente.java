package com.github.teresafernandes.projetojsf.modelo;

import java.io.Serializable;

/**
 * @author Teresa Fernandes
 * */

public interface EntidadePersistente extends Serializable {

	public Integer getId();
	public EntidadePersistente clone();
}
