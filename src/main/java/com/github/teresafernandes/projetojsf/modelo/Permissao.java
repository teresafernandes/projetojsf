package com.github.teresafernandes.projetojsf.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Teresa Fernandes
 * */

@Entity
@Table(name="permissao")
public class Permissao implements EntidadePersistente {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)	
	@Column(name="id_permissao")
	private Integer id;
	private String nome;
	private boolean ativo;
	
	public Permissao() {
		
	}

	public Integer getId() {
		if(id == null) return 0;
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Permissao other = (Permissao) obj;
		if (id != other.id)
			return false;
		return true;
	}

	public Permissao clone() {
		Permissao p = new Permissao();
		p.setId(id);
		p.setNome(nome);
		p.setAtivo(ativo);
		return p;
	}
	
}
