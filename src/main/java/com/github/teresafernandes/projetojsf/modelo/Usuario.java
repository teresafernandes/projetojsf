package com.github.teresafernandes.projetojsf.modelo;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.validator.constraints.Email;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * @author Teresa Fernandes
 * */

@SuppressWarnings("deprecation")
@Entity
@Table(name="usuario")
public class Usuario implements EntidadePersistente, UserDetails {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id_usuario")
	private Integer id;
	private String nome;
	@Email
	private String email;
	private String login;
	private String senha;
	private boolean ativo;
	@Column(name="data_cadastro")
	private Date dataCadastro;
	@JoinColumn(name="id_perfil")
	@ManyToOne
	private Perfil perfil;

	public Integer getId() {
		if(id == null) return 0;
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}
	
	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Perfil getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}

	@Override
	@Transient
	public Collection<? extends GrantedAuthority> getAuthorities() {
		Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
		if(getPerfil() != null){
			for(Permissao p: getPerfil().getPermissoes())
				authorities.add(new GrantedAuthorityImpl(p.getNome()));
		}
//		authorities.add(new GrantedAuthorityImpl("ROLE_USER"));
		return authorities;
	}

	@Override
	@Transient
	public String getPassword() {
		return senha;
	}

	@Override
	@Transient
	public String getUsername() {
		return login;
	}

	@Override
	@Transient
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	@Transient
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	@Transient
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	@Transient
	public boolean isEnabled() {
		return ativo;
	}

	public Usuario clone() {
		Usuario u = new Usuario();
		u.setId(id);
		u.setNome(nome);
		u.setLogin(login);
		u.setEmail(email);
		u.setAtivo(ativo);
		u.setSenha(senha);
		u.setDataCadastro(dataCadastro);
		return u;
	}
}
