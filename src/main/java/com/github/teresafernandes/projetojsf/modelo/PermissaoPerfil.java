package com.github.teresafernandes.projetojsf.modelo;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Teresa Fernandes
 * */

@Entity
@Table(name="permissao_perfil")
public class PermissaoPerfil implements EntidadePersistente {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id_permissao_perfil")
	private Integer id;	
	@JoinColumn(name="id_permissao")
	@ManyToOne
	private Permissao permissao;	
	@JoinColumn(name="id_perfil")
	@ManyToOne
	private Perfil perfil;	
	private boolean ativo;	
	@Column(name="data_cadastro")
	private Date dataCadastro;
	
	public PermissaoPerfil() {
		
	}
	
	public PermissaoPerfil(Permissao permissao) {
		this.permissao = permissao;
	}

	public Integer getId() {
		if(id == null) return 0;
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Permissao getPermissao() {
		return permissao;
	}

	public void setPermissao(Permissao permissao) {
		this.permissao = permissao;
	}

	public Perfil getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}
 
	public PermissaoPerfil clone() {
		PermissaoPerfil p = new PermissaoPerfil();
		p.setId(id);
		p.setPerfil(perfil);
		p.setPermissao(permissao);
		p.setAtivo(ativo);
		return p;
	}
}
