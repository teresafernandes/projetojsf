package com.github.teresafernandes.projetojsf.modelo;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * @author Teresa Fernandes
 * */

@Entity
@Table(name="perfil")
public class Perfil implements EntidadePersistente {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)	
	@Column(name="id_perfil")
	private Integer id;
	private String nome;
	private boolean ativo;
	@Transient
	private List<Permissao> permissoes;
	
	public Perfil() {
		setPermissoes(new ArrayList<Permissao>());
		setAtivo(true);
	}

	public Integer getId() {
		if(id == null) return 0;
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Perfil other = (Perfil) obj;
		if (id != other.id)
			return false;
		return true;
	}

	public Perfil clone() {
		Perfil p = new Perfil();
		p.setId(id);
		p.setNome(nome);
		p.setAtivo(ativo);
		return p;
	}

	public List<Permissao> getPermissoes() {
		return permissoes;
	}

	public void setPermissoes(List<Permissao> permissoes) {
		this.permissoes = permissoes;
	}
	
}
