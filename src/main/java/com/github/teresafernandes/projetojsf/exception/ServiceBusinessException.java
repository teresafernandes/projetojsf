
package com.github.teresafernandes.projetojsf.exception;

/**
 * @author Teresa Fernandes
 * */

public class ServiceBusinessException extends Exception {

    private static final long serialVersionUID = 1L;

    public ServiceBusinessException(String message) {
        super(message);
    }


}
