package com.github.teresafernandes.projetojsf.exception;

/**
 * @author Teresa Fernandes
 * */

public class DataAccessExceptionTranslator {

    /**
     * Traduz a mensagem de erro de forma a torná-la mais amigável.
     * 
     * @param dae a exceção
     * @return a exeção com uma mensagem de erro amigável
     */
    public static DataAccessException translate(
            org.springframework.dao.DataAccessException dae) {
        dae.printStackTrace();
        String msgExcecaoTraduzida = "";
        String msgExcecaoOriginal = dae.getMessage().toLowerCase();

        if (dae instanceof org.springframework.dao.DataIntegrityViolationException) {
            msgExcecaoTraduzida = "A operação não pode ser realizada por violar a integridade dos dados";

            // DuplicateKeyException é filha de DataIntegrityViolationException.
            if (dae instanceof org.springframework.dao.DuplicateKeyException) {
                msgExcecaoTraduzida = "Operação não permitida pois este registro tem de ser único.";
            } else if (msgExcecaoOriginal.contains("could not delete")) {
                msgExcecaoTraduzida = "O registro não pode ser removido por haver registros dependentes";
            }
        }

        msgExcecaoTraduzida = msgExcecaoTraduzida.trim();

        if (msgExcecaoTraduzida.length() > 0) {
            return new DataAccessException(msgExcecaoTraduzida, dae);
        } else {
            return new DataAccessException(dae);
        }
    }

}
