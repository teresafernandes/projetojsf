
package com.github.teresafernandes.projetojsf.exception;

import org.springframework.dao.DataAccessException;

/**
 * @author Teresa Fernandes
 * */

public class ServiceDataAccessException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public ServiceDataAccessException(String message) {
        super(message);
    }

	public ServiceDataAccessException(String message, DataAccessException e) {
		super(message,e);
	}

	public ServiceDataAccessException(DataAccessException e) {
		super(e);
	}


}
