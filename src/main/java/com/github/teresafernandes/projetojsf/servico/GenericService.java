package com.github.teresafernandes.projetojsf.servico;

import java.io.Serializable;
import java.util.List;

import com.github.teresafernandes.projetojsf.exception.ServiceBusinessException;
import com.github.teresafernandes.projetojsf.modelo.EntidadePersistente;

/**
 * @author Teresa Fernandes
 * */

public interface GenericService<T extends EntidadePersistente> extends Serializable {

    public abstract Class<T> getClassePersistente();
    
    public abstract void salvar(T entity) throws ServiceBusinessException;
    
    public abstract T buscarPorId(Integer id);
    
    public abstract List<T> listarTodos();
    
    public abstract void excluir(T entity);
    
    public abstract void validar(T entity) throws ServiceBusinessException;

    public abstract List<T> buscarPorAtributosEntidade(T objBusca, int first, int pageSize, String sortField, boolean sortOrder) ;
    
    public abstract Long buscarContadorPorAtributosEntidade(T objBusca) ;
    
}