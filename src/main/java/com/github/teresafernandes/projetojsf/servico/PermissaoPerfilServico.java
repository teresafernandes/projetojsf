package com.github.teresafernandes.projetojsf.servico;

import java.util.List;

import com.github.teresafernandes.projetojsf.modelo.Perfil;
import com.github.teresafernandes.projetojsf.modelo.PermissaoPerfil;

/**
 * @author Teresa Fernandes
 * */

public interface PermissaoPerfilServico extends GenericService<PermissaoPerfil>{

	public List<PermissaoPerfil> obterPermissoesPeloPerfil(Perfil perfil);
}
