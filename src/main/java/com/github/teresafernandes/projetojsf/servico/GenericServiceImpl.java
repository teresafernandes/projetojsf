
package com.github.teresafernandes.projetojsf.servico;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.github.teresafernandes.projetojsf.dao.GenericDAO;
import com.github.teresafernandes.projetojsf.exception.ServiceBusinessException;
import com.github.teresafernandes.projetojsf.exception.ServiceDataAccessException;
import com.github.teresafernandes.projetojsf.modelo.EntidadePersistente;

/**
 * @author Teresa Fernandes
 * */

public abstract class GenericServiceImpl<T extends EntidadePersistente, D extends GenericDAO<T>> implements GenericService<T> {

	private static final long serialVersionUID = 1L;

	protected final D dao;

	public GenericServiceImpl(D dao) {
		this.dao = dao;
	}

	@Override
    public void excluir(T entity) {
		try {
			dao.excluir(entity);
		} catch (DataAccessException e) {
			throw new ServiceDataAccessException(e.getMessage(), e);
		}
	}

	@Override
    public List<T> listarTodos() {
	    try {
	        return dao.listarTodos();
        } catch (DataAccessException e) {
            throw new ServiceDataAccessException(e);
        }
	}

	@Override
    public T buscarPorId(Integer id) {
	    try {
	        return dao.buscarPorId(id);
        } catch (DataAccessException e) {
            throw new ServiceDataAccessException(e);
        }
	}

	@Override
    public Class<T> getClassePersistente() {
		return dao.getClassePersistente();
	}

    @Override
    public void salvar(T entity) throws ServiceBusinessException {
        try {
        	validar(entity);
            dao.salvar(entity);
        } catch (DataAccessException e) {
            throw new ServiceDataAccessException(e);
        }
    }
    
    public void validar(T entity) throws ServiceBusinessException{}
    
    public List<T> buscarPorAtributosEntidade(T objBusca, int first, int pageSize, String sortField, boolean sortOrder){
    	return dao.buscarPorAtributosEntidade(objBusca, first, pageSize, sortField, sortOrder);
    }
    
    public Long buscarContadorPorAtributosEntidade(T objBusca){
    	return dao.buscarContadorPorAtributosEntidade(objBusca);
    }
}
