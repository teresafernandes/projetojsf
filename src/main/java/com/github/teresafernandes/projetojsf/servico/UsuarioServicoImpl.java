package com.github.teresafernandes.projetojsf.servico;

import javax.inject.Inject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.teresafernandes.projetojsf.dao.UsuarioDAO;
import com.github.teresafernandes.projetojsf.exception.ServiceBusinessException;
import com.github.teresafernandes.projetojsf.modelo.Usuario;
import com.github.teresafernandes.projetojsf.util.ValidatorUtil;

/**
 * @author Teresa Fernandes
 * */

@Service("usuarioServico")
@Transactional
public class UsuarioServicoImpl extends GenericServiceImpl<Usuario, UsuarioDAO> implements UsuarioServico{

	private static final long serialVersionUID = 1L;

	@Inject
	public UsuarioServicoImpl(UsuarioDAO dao) {
		super(dao);
	}

	@Autowired
	private PasswordEncoder passwordEncoder;
	@Autowired
	private PermissaoPerfilServico permissaoUsuarioServico;
	
	public Usuario obterUsuarioPeloLogin(String login) {
		return dao.obterUsuarioPeloLogin(login);
	}
	
	@Override
	public void salvar(Usuario usuario) throws ServiceBusinessException {
		usuario.setLogin(usuario.getNome());
		validar(usuario);
		dao.salvar(usuario);
	}

	@Override
	public void validar(Usuario usuario) throws ServiceBusinessException {
		if(usuario == null)
			throw new ServiceBusinessException("Entidade persistente inválida.");
		if(ValidatorUtil.isEmpty(usuario.getNome()))
			throw new ServiceBusinessException("Nome: Campo obrigatório.");
		
		Usuario usuarioSalvo = obterUsuarioPeloLogin(usuario.getLogin());
		if(usuarioSalvo != null && 
				usuarioSalvo.getId() != usuario.getId()){
			throw new ServiceBusinessException("Já existe um usuário cadastrado com este email");
		}		
	}
}