package com.github.teresafernandes.projetojsf.servico;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.teresafernandes.projetojsf.dao.PermissaoPerfilDAO;
import com.github.teresafernandes.projetojsf.modelo.Perfil;
import com.github.teresafernandes.projetojsf.modelo.PermissaoPerfil;

/**
 * @author Teresa Fernandes
 * */

@Service("permissaoPerfilServico")
@Transactional
public class PermissaoPerfilServicoImpl extends GenericServiceImpl<PermissaoPerfil, PermissaoPerfilDAO> implements PermissaoPerfilServico{
	
	private static final long serialVersionUID = 1L;

	@Inject
	public PermissaoPerfilServicoImpl(PermissaoPerfilDAO dao) {
		super(dao);
	}
	public List<PermissaoPerfil> obterPermissoesPeloPerfil(Perfil perfil){
		return dao.obterPermissoesPeloPerfil(perfil);
	}

}