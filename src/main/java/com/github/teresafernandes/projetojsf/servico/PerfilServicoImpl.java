package com.github.teresafernandes.projetojsf.servico;

import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.teresafernandes.projetojsf.dao.PerfilDAO;
import com.github.teresafernandes.projetojsf.exception.ServiceBusinessException;
import com.github.teresafernandes.projetojsf.modelo.Perfil;
import com.github.teresafernandes.projetojsf.modelo.Permissao;
import com.github.teresafernandes.projetojsf.modelo.PermissaoPerfil;
import com.github.teresafernandes.projetojsf.util.ValidatorUtil;

/**
 * @author Teresa Fernandes
 * */

@Service("perfilServico")
@Transactional
public class PerfilServicoImpl extends GenericServiceImpl<Perfil, PerfilDAO> implements PerfilServico{
	
	private static final long serialVersionUID = 1L;

	@Autowired
	private PermissaoPerfilServico permissaoPerfilServico;
	
	@Inject
	public PerfilServicoImpl(PerfilDAO dao) {
		super(dao);
	}
	
	@Override
	public void salvar(Perfil entity) throws ServiceBusinessException {
		validar(entity);
		super.salvar(entity);
		vincularPermissao(entity);
	}
	
	private void vincularPermissao(Perfil perfil) throws ServiceBusinessException{
		if(perfil.getPermissoes() != null){
			List<PermissaoPerfil> permissoesBd = permissaoPerfilServico.obterPermissoesPeloPerfil(perfil);
			boolean existe = false;
			for(Permissao p: perfil.getPermissoes()){
				existe = false;
				for(PermissaoPerfil pbd: permissoesBd){
					if(pbd.getPermissao().equals(p))
						existe = true;
				}
				if(!existe){
					PermissaoPerfil pp = new PermissaoPerfil();
					pp.setPerfil(perfil);
					pp.setAtivo(true);
					pp.setDataCadastro(new Date());
					pp.setPermissao(p);
					permissaoPerfilServico.salvar(pp);
				}
			}
			
			for(PermissaoPerfil pbd: permissoesBd){
				existe = false;
				for(Permissao p: perfil.getPermissoes()){
					if(pbd.getPermissao().equals(p))
						existe = true;
				}
				if(!existe)
					permissaoPerfilServico.excluir(pbd);
			}
		}
	}
	
	@Override
	public void validar(Perfil entity) throws ServiceBusinessException {
		if(entity == null)
			throw new ServiceBusinessException("Entidade persistente inválida.");
		if(ValidatorUtil.isEmpty(entity.getNome()))
			throw new ServiceBusinessException("Nome: Campo obrigatório.");
		if(ValidatorUtil.isEmpty(entity.getPermissoes()))
			throw new ServiceBusinessException("Adicione ao menos uma permissão ao perfil.");
		if(dao.countPerfisPeloNome(entity) > 0)
			throw new ServiceBusinessException("Já existe um perfil cadastrado com o nome '"+entity.getNome()+"'.");
	}

}