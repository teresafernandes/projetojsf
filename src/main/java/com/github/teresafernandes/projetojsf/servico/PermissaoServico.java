package com.github.teresafernandes.projetojsf.servico;

import java.util.List;

import com.github.teresafernandes.projetojsf.modelo.Perfil;
import com.github.teresafernandes.projetojsf.modelo.Permissao;
import com.github.teresafernandes.projetojsf.modelo.Usuario;

/**
 * @author Teresa Fernandes
 * */

public interface PermissaoServico extends GenericService<Permissao>{

	public List<Permissao> obterPermissoesPeloUsuario(Usuario usuario);
	public List<Permissao> obterPermissoesPeloPerfil(Perfil perfil);
}
