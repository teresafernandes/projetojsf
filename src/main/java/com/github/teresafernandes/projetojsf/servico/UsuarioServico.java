package com.github.teresafernandes.projetojsf.servico;

import com.github.teresafernandes.projetojsf.modelo.Usuario;

/**
 * @author Teresa Fernandes
 * */

public interface UsuarioServico extends GenericService<Usuario> {

	public Usuario obterUsuarioPeloLogin(String login);
}
