package com.github.teresafernandes.projetojsf.servico;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.teresafernandes.projetojsf.dao.PermissaoDAO;
import com.github.teresafernandes.projetojsf.exception.ServiceBusinessException;
import com.github.teresafernandes.projetojsf.modelo.Perfil;
import com.github.teresafernandes.projetojsf.modelo.Permissao;
import com.github.teresafernandes.projetojsf.modelo.Usuario;
import com.github.teresafernandes.projetojsf.util.ValidatorUtil;

/**
 * @author Teresa Fernandes
 * */

@Service("permissaoServico")
@Transactional
public class PermissaoServicoImpl extends GenericServiceImpl<Permissao, PermissaoDAO> implements PermissaoServico{
	
	private static final long serialVersionUID = 1L;

	@Inject
	public PermissaoServicoImpl(PermissaoDAO dao) {
		super(dao);
	}

	@Override
	public void salvar(Permissao permissao) throws ServiceBusinessException {
		validar(permissao);
		if(ValidatorUtil.isEmpty(permissao.getId()))
			permissao.setAtivo(true);
		dao.salvar(permissao);
	}

	@Override
	public void validar(Permissao permissao) throws ServiceBusinessException {
		if(permissao == null)
			throw new ServiceBusinessException("Entidade persistente inválida.");
		if(ValidatorUtil.isEmpty(permissao.getNome()))
			throw new ServiceBusinessException("Nome: Campo obrigatório.");
		if(dao.countPermissoesPeloNome(permissao) > 0)
			throw new ServiceBusinessException("Já existe uma permissão cadastrada com o nome '"+permissao.getNome()+"'");
	}
	
	@Override
	public List<Permissao> listarTodos() {
		return dao.listarTodos();		
	}

	@Override
	public List<Permissao> obterPermissoesPeloUsuario(Usuario usuario){
		return dao.obterPermissoesPeloUsuario(usuario);
	}
	
	public List<Permissao> obterPermissoesPeloPerfil(Perfil perfil){
		return dao.obterPermissoesPeloPerfil(perfil);
	}

}