package com.github.teresafernandes.projetojsf.servico;

import com.github.teresafernandes.projetojsf.modelo.Perfil;

/**
 * @author Teresa Fernandes
 * */

public interface PerfilServico extends GenericService<Perfil>{

}
